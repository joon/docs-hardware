=====================
 Dell Latitude E6400
=====================

Graphics
========

* The driver comes with Windows 8 does not have 3D support so impressive does
  not work.

`How To: Install "legacy" Intel HD Graphics drivers on Windows 8
<http://www.neowin.net/news/how-to-install-legacy-intel-hd-graphics-drivers-on-windows-8>`_

* Mobile Intel(R) 4 Series Express Chipset Family

  * Download 15.17.19.64.2869 (11/16/2012) which contains 8.15.10.2869 (10/4/2012) from
    https://downloadcenter.intel.com/Detail_Desc.aspx?agr=Y&DwnldID=22167&lang=eng&OSVersion=Windows%207%20(64-bit)*&DownloadType=Drivers
    (Win7Vista_64_151719.exe)

  * It seems like zooming makes things slow

CPU Throttling
==============

http://www.xtremesystems.org/forums/showthread.php?240717-Dell-Latitude-E6500-E6400-ThrottleGate-Fix

http://www.fileden.com/files/2008/3/3/1794507/ThrottleStop.zip


