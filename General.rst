=========
 General
=========

CPU
===

www.cpubenchmark.net

* `Intel Pentium G2020 @ 2.90GHz
  <http://www.cpubenchmark.net/cpu.php?cpu=Intel+Pentium+G2020+%40+2.90GHz>`_:
  2797
* `Intel Pentium G645T @ 2.50GHz
  <http://www.cpubenchmark.net/cpu.php?cpu=Intel+Pentium+G645T+%40+2.50GHz>`_:
  2496 

Ram
===

Search for ``crucial <model> ram upgrade`` in Bing or Google. The Crucial
website will tell you which ram modules are compatible with the model. e.g.::

   crucial m72e ram upgrade



