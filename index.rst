.. Hardware Documentation documentation master file, created by
   sphinx-quickstart on Sun Jun 09 12:14:02 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Hardware Documentation's documentation!
==================================================

Contents:

.. toctree::
   :maxdepth: 1
   
   General
   dell-latitude-e6400
   logitech-webcams


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

